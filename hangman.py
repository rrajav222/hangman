import random
from hangman_art import stages,logo
from hangman_words import word_list

chosen_word = random.choice(word_list)

# print(f'Pssst, the solution is {chosen_word}.')
def game():
  print("Welcome to Hangman")
  print(logo)
  display =[]
  for _ in chosen_word:
    display.append("_")
  print(display)

  remaining_lives = 6
  won = False 
  while remaining_lives > 0 and won == False:
    guess = input("\nGuess a letter: ").lower()
    if len(guess) == 1 and guess in chosen_word:
      print("Correct Guess")
      for position in range(len(chosen_word)):
        if chosen_word[position] == guess:
            display[position] = guess
      print(display)

    else:
      print("Wrong Guess")
      remaining_lives -= 1
      print(stages[remaining_lives])
      print(f"remaining lives: {remaining_lives}")
      print(display)

    if "_" not in display: 
      won = True
      print("Congratulations!! You have won")

  if remaining_lives == 0:
    print("Game over!! Try again")
    print(f"The correct answer is {chosen_word}")

  play = input("Wanna play again? Y/N: ").upper()
  if play == "Y":
    game()

game()